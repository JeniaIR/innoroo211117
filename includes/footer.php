<?php 

    echo '

    <footer class="footer_bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-3  paddingtop-bottom">
                        <h6 class="heading_footer">About</h6>
                        <ul class="footer-ul">
                            <li  class="li_list_footer"><a href="about/team/" class="list_footer">Team</a></li>
                            <li  class="li_list_footer"><a href="about/trainers/" class="list_footer">Trainers</a></li>
                            <li  class="li_list_footer"><a href="careers/" class="list_footer">Careers</a></li>
                           <!-- <li  class="li_list_footer"><a href="#" class="list_footer">Solutions</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">partnership</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">Case Studies</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">Achievement</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">Careers</a></li>-->
                        </ul>
                    </div>
                      <div class="col-md-2 col-sm-3  footerleft ">
                          <h6 class="heading_footer">Offerings</h6>
                          <ul class="footer-ul">
                              <li  class="li_list_footer"><a href="offerings/scaled-agile-framework/" class="list_footer"> SAFe</a></li>
                             <!-- <li  class="li_list_footer"><a href="#" class="list_footer"> Scrum</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer"> Kanban</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer"> DevOps</a></li>-->
  
                          </ul>
  
                      </div>
                      <div class="col-md-2 col-sm-3 paddingtop-bottom">
                          <h6 class="heading_footer">Resources</h6>
                          <ul class="footer-ul">
                              <li  class="li_list_footer"><a href="blog/" class="list_footer">Blog</a></li>
                              
  
                          </ul>
                      </div>
  
                       <div class="col-md-3 col-sm-3 paddingtop-bottom">
                        <h6 class="heading_footer">Policies</h6>
                        <ul class="footer-ul">

                       
                            <li  class="li_list_footer"><a href="policy/disclaimer/" class="list_footer">Disclaimer</a></li>
  <li  class="li_list_footer"><a href="policy/refund-policy" class="list_footer">Cancellation &amp; Refund Policy</a></li>

                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12  paddingtop-bottom">
<h6 class="heading_footer">Location</h6>
                        <!--<h6 class="heading_footer">Registered Office</h6>-->
                        
                        <p class="list_footer_add"><strong>Registered Office</strong><br>
                         
                          No 43, Van Marg,<br> Banapura, Madhya Pradesh,<br>India - 461221

                        </p><br />
                        <!--<h6 class="heading_footer">Bengaluru Office</h6>-->
                        
                       
                        <p class="list_footer_add"><strong>Bengaluru Office</strong><br>
                            
			     No.94, 17/1, 2nd Floor, <br> Ambalipura, Bellandur Gate, <br>
                                        Sarjapura Main Road,<br>Bengaluru, Karnataka,<br>India - 560102
                                    
                        </p>
                        

                    </div>
                    
                    <!--<div class="col-md-3 col-sm-12  paddingtop-bottom">
                        <h6 class="heading_footer">Bengaluru Office</h6>
                        
                       
                        <p class="list_footer_add">
                            
			     No.94, 17/1, 2nd Floor, <br> Ambalipura, Bellandur Gate, <br>
                                        Sarjapura Main Road, <br>Bengaluru, Karnataka,<br>India 560102
                                    
                        </p>

                    </div>-->
                </div>
            </div>
        </footer>
        <!--footer start from here-->

        <div class="copyright">
            <div class="container">
<div class="row">
                <div class="col-md-6 col-xs-12">
                    <p class="copy_right_txt">© 2013- 2017 INNOVATION ROOTS SOFTECH PVT LTD</p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <ul class="bottom_ul">
 <li class="bottom_ul_li"><a style="text-decoration: none;" href="https://www.meetup.com/innoroo/" target="_blank"><i class="fa fa-meetup bottom_ul_li_a meetup_footer_hover" aria-hidden="true"></i></a></li>
                        <li class="bottom_ul_li"><a style="text-decoration: none;" href="https://www.facebook.com/innoroo/" target="_blank"><i class="fa fa-facebook bottom_ul_li_a fb_footer_hover" aria-hidden="true"></i></a></li>
                        <li class="bottom_ul_li"><a style="text-decoration: none;" href="https://twitter.com/innoroo" target="_blank"><i class="fa fa-twitter bottom_ul_li_a twi_footer_hover" aria-hidden="true"></i></a></li>
                        <li class="bottom_ul_li"><a style="text-decoration: none;" href="https://www.linkedin.com/company-beta/3720949/" target="_blank"><i class="fa fa-linkedin bottom_ul_li_a li_footer_hover" aria-hidden="true"></i></a></li>
                        <li class="bottom_ul_li"><a style="text-decoration: none;" href="https://plus.google.com/u/0/105968218237342764016" target="_blank"><i class="fa fa-google-plus bottom_ul_li_a gplus_footer_hover" aria-hidden="true"></i></a></li>
                        
                        <li class="bottom_ul_li"><a style="text-decoration: none;" href="https://in.pinterest.com/innovationroots/pins/" target="_blank"><i class="fa fa-pinterest bottom_ul_li_a pint_footer_hover" aria-hidden="true"></i></a></li>
                        
                        <li class="bottom_ul_li"><a style="text-decoration: none;" href="https://www.instagram.com/innoroo/" target="_blank"><i class="fa fa-instagram bottom_ul_li_a insta_footer_hover" aria-hidden="true"></i></a></li>
                        

                    </ul>
                </div>
</div>
            </div>
        </div>
        
        <button onclick="topFunction()" id="myBtn" title="Go to top" style="display: block;"><i class="fa fa-chevron-up fa_back_top scrollToTop" aria-hidden="true"></i></button>
        <script>

            window.onscroll = function () {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("myBtn").style.display = "block";
                } else {
                    document.getElementById("myBtn").style.display = "none";
                }
            }


            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>
    ';

    ?>

