<?php

$hasError = FALSE;
$mailSent = FALSE;

if (isset($_POST['formSubmit'])) {
    // get the posted data    
    $name = trim(htmlspecialchars($_POST['name'], ENT_QUOTES));
    $email = trim($_POST['email']);
    $subLine = trim(htmlspecialchars($_POST['subject'], ENT_QUOTES));
    $message = trim(htmlspecialchars($_POST['message'], ENT_QUOTES));
    $subscribe = $_POST['newsletter'];

    $fieldsArray = array(
        'name' => $name,
        'email' => $email,
        'subject' => $subLine,
        'message' => $message,
        'newsletter' => $subscribe
    );

    $errorArray = array();

    foreach ($fieldsArray as $key => $value) {
        switch ($key) {
            case 'name':
                if ($name != "") {
                    $name = filter_var($name, FILTER_SANITIZE_STRING);
                    if ($name == "") {
                        $hasError = TRUE;
                        $errorArray[$key] = "Please enter Name Correctly";
                    }
                } else {
                    $hasError = TRUE;
                    $errorArray[$key] = "Please enter your Name";
                }
                break;

            case 'email':
                if (!filter_var($email, FILTER_VALIDATE_EMAIL) || $email == "") {
                    $hasError = TRUE;
                    $errorArray[$key] = "Invalid Email address entered";
                } else {
                    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
                }
                break;
            case 'subject':
                if ($subLine != "") {
                    $subLine = filter_var($subLine, FILTER_SANITIZE_STRING);
                    if ($subLine == "") {
                        $hasError = TRUE;
                        $errorArray[$key] = "Please enter a valid subject line";
                    }
                } else {
                    $hasError = TRUE;
                    $errorArray[$key] = "Please enter a subject line";
                }
                break;
            case 'message':
                if ($message != "") {
                    $message = filter_var($message, FILTER_SANITIZE_STRING);
                    if ($message == "") {
                        $hasError = TRUE;
                        $errorArray[$key] = "Please enter a message to send";
                    }
                } else {
                    $hasError = TRUE;
                    $errorArray[$key] = "Please enter a message to send";
                }
                break;
            case 'newsletter':
                break;
        }
    }

    if ($hasError != TRUE) {
        $to = 'community@innovationroots.com';
        $subject = $subLine;
        $msgContent = "<b>From:</b>&nbsp;$name<br>"
                . "<b>Email:</b>&nbsp;$email<br>"
                . "<b>Subject:</b>&nbsp;$subLine<br>"
                . "<b>Message:</b>&nbsp;$message<br>"
                . "<b>Subscription:</b>&nbsp;$subscribe";

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: $name <$email> \r\n";
        $headers .= "Reply-to: $name <$email> \r\n";
        //Additional Cc & Bcc inclusion
        $headers .= "Bcc: Contact <info@innovationroots.com>" . "\r\n";

        // send the email  
        mail($to, $subject, $msgContent, $headers);
    }
}
?>