<!DOCTYPE html>
<html class="full" lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>INNOVATION ROOTS | Resources | Library</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../../css/style.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.ico">

        <!-- google fonts-->

        <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



        <!--<script type="text/javascript">

            document.oncontextmenu = new Function("return false");
            document.onselectstart = new Function("return false");
            if (window.sidebar) {
                document.onmousedown = new Function("return false");
                document.onclick = new Function("return true");

                document.oncut = new Function("return false");

                document.oncopy = new Function("return false");


                document.onpaste = new Function("return false");
            }
        </script>-->
        <script type="text/javascript">
            /* $('.content').hide();
             
             $('.slider').click(function () {
             $(this).next('.content').slideToggle();
             this.toggle = !this.toggle;
             $(this).children("span").text(this.toggle ? "[-]" : "[+]");
             return false;
             });
             */
            $(".slider").hover(function () {
                $(this).children('.content').slideToggle("slow");
            });

//$('.content').hide();
        </script>
    </head>

    <body>

        <header>
            <div class="container-fluid main">

                <nav class="navbar navbar-default navbar-fixed-top" style="position: fixed ;">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle toggle_edit_mobile" data-toggle="collapse" data-target="#ir_nav">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span> 
                            </button>
                            <a class="navbar-brand logo_innoroo " href="../../"><img src="../../img/logo.png" class="logo_img img-responsive"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="ir_nav">
                            <ul class="nav navbar-nav ul_list_header">
                                <li ><a href="../../training/" class="li_list_header">Training</a></li>
                                <li ><a href="../../career/" class="li_list_header">Career</a></li>
                                <li ><a href="../../contact/" class="li_list_header">Contact</a></li>

                            </ul>
                        </div>
                    </div>
                </nav>



            </div>
        </header>

        <section class="page-cover_bg" style=""> 
            <!--<div class="casestudy_bg page_bg bg_overlay"></div>-->
            <div class="container">
                <div class="page_center_caption">
                    <h1 class="text_center">Library</h1>
                    <div class="text_center_hr"></div>
                    <h2 class="heading_sub_page">Open for All</h2>

                </div>


            </div>
        </section>  
        <section class="grey_bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="intro_library">We have 100 plus articles available for Reference & Reading. You may visit us with prior notice</h3>
                    </div>
                </div>
            </div>
        </section>

        <section class="section_margin_gen">
            <div class="container">
                <div class="row">

                </div>
            </div>
        </section>


        <footer class="footer_bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-3  paddingtop-bottom">
                        <h6 class="heading_footer">About</h6>
                        <ul class="footer-ul">
                            <li  class="li_list_footer"><a href="../../about/team/" class="list_footer">Team</a></li>
                            <li  class="li_list_footer"><a href="../../about/trainers/" class="list_footer">Trainer</a></li>
                            <li  class="li_list_footer"><a href="../../about/casestudy/" class="list_footer">Case Studies</a></li>
                            <!--<li  class="li_list_footer"><a href="#" class="list_footer">Solutions</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">partnership</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">Case Studies</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">Achievement</a></li>
                            <li  class="li_list_footer"><a href="#" class="list_footer">Careers</a></li>-->
                        </ul>
                    </div>
                    <!--  <div class="col-md-2 col-sm-3  footerleft ">
                          <h6 class="heading_footer">Offerings</h6>
                          <ul class="footer-ul">
                              <li  class="li_list_footer"><a href="#" class="list_footer"> SAFe</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer"> Scrum</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer"> Kanban</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer"> DevOps</a></li>
  
                          </ul>
  
                      </div>
                      <div class="col-md-2 col-sm-3 paddingtop-bottom">
                          <h6 class="heading_footer">Resources</h6>
                          <ul class="footer-ul">
                              <li  class="li_list_footer"><a href="#" class="list_footer">Blogs</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer">Essays</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer">Games</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer">Videos</a></li>
                              <li  class="li_list_footer"><a href="#" class="list_footer">Presentations</a></li>
  
                          </ul>
                      </div>-->

                    <div class="col-md-2 col-sm-3 paddingtop-bottom">
                        <h6 class="heading_footer">Policies</h6>
                        <ul class="footer-ul">

                            <!-- <li  class="li_list_footer"><a href="#" class="list_footer">Terms & Conditions</a></li>-->
                            <li  class="li_list_footer"><a href="../../Policy/disclaimer/" class="list_footer">Disclaimer</a></li>

                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12  paddingtop-bottom">
                        <h6 class="heading_footer">Location</h6>
                        <!--  <iframe style="margin-bottom: 6%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7777.61650766879!2d77.66201302439633!3d12.920042213558876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae137155758401%3A0xdd27628f4bc9dfb3!2sInnovation+Roots+Softech+Pvt+Ltd.!5e0!3m2!1sen!2sin!4v1492767246408" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                        <p class="list_footer_add"><strong>Registered Office</strong>
                            <br/>43, Van Marg,<br> Banapura, Madhya Pradesh<br>India - 461221

                        </p>
                        <p class="list_footer_add"><strong>Bangalore Office</strong><br>
                            No.94, 17/1, 2nd Floor,<br>
                            Ambalipura, Bellandur Gate,<br>
                            Sarjapura Main Road,<br>
                            Bangalore, Karnataka<br>India -560102<br>

                        </p>

                    </div>
                </div>
            </div>
        </footer>
        <!--footer start from here-->

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <p class="copy_right_txt">© 2013- 2017 INNOVATION ROOTS SOFTECH PVT LTD</p>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <ul class="bottom_ul">

                            <li class="bottom_ul_li"><a style="text-decoration: none;" target="blank" href="https://www.facebook.com/innoroo/"><i class="fa fa-facebook bottom_ul_li_a fb_footer_hover" aria-hidden="true"></i></a></li>
                            <li class="bottom_ul_li"><a style="text-decoration: none;" target="blank" href="https://twitter.com/innoroo"><i class="fa fa-twitter bottom_ul_li_a twi_footer_hover" aria-hidden="true"></i></a></li>
                            <li class="bottom_ul_li"><a style="text-decoration: none;" target="blank" href="https://www.linkedin.com/company-beta/3720949/"><i class="fa fa-linkedin bottom_ul_li_a li_footer_hover" aria-hidden="true"></i></a></li>
                            <li class="bottom_ul_li"><a style="text-decoration: none;" target="blank" href="https://plus.google.com/u/0/105968218237342764016"><i class="fa fa-google-plus bottom_ul_li_a gplus_footer_hover" aria-hidden="true"></i></a></li>
                            <li class="bottom_ul_li"><a style="text-decoration: none;" target="blank" href="https://in.pinterest.com/innovationroots/pins/"><i class="fa fa-pinterest bottom_ul_li_a pint_footer_hover" aria-hidden="true"></i></a></li>
                            <li class="bottom_ul_li"><a style="text-decoration: none;" target="blank" href="https://www.instagram.com/innoroo/"><i class="fa fa-instagram bottom_ul_li_a insta_footer_hover" aria-hidden="true"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-chevron-up fa_back_top" aria-hidden="true"></i></button>

        <script>

            window.onscroll = function () {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("myBtn").style.display = "block";
                } else {
                    document.getElementById("myBtn").style.display = "none";
                }
            }


            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>
        <!-- jQuery -->
        <script src="../../js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../js/bootstrap.min.js"></script>

    </body>

</html>
