<!DOCTYPE html>
<html class=full lang=en>
<head>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=viewport content="width=device-width, initial-scale=1">
<meta name=description content>
<meta name=author content>
<title>INNOVATION ROOTS | Training | PMI- Agile Certified Practitioner (PMI-ACP)&reg;</title>
<link href=https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css rel=stylesheet>
<link href=/inc/assets/css/normalize-3.0.2.css rel=stylesheet />
<link href=/inc/assets/css/animate.css rel=stylesheet />
<link rel=stylesheet href=/inc/vendor/owl-carousel/owl.carousel.css>
<link rel=stylesheet href=/inc/vendor/owl-carousel/owl.theme.css>
<link rel=stylesheet href=https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/less/glyphicons.less />
<link rel=stylesheet href=/inc/assets/css/material-design-iconic-font.min.css />
<link rel=stylesheet href=https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css>
<link href="//fonts.googleapis.com/css?family=Roboto+Slab:400,300,700|Open+Sans:700,400,300|Playfair+Display:400,400italic" rel=stylesheet />
<link href=/inc/assets/css/main.css rel=stylesheet />
<script src=/inc/assets/js/modernizr.custom.83079.js></script>
<link rel="shortcut icon" type=image/x-icon href=/inc/assets/img/favicon.ico>
<link rel=stylesheet href=/inc/assets/css/material-design-iconic-font.min.css />
<link href="//fonts.googleapis.com/css?family=Roboto+Slab:400,300,700|Open+Sans:700,400,300|Playfair+Display:400,400italic" rel=stylesheet />
<link href=/inc/assets/css/main.css rel=stylesheet />
<script src=/inc/assets/js/modernizr.custom.83079.js></script>
<link rel="shortcut icon" type=image/x-icon href=/inc/assets/img/favicon.ico>
<script src=https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js></script>
<script src=https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js></script>
<script>(function(d,e,j,h,f,c,b){d.GoogleAnalyticsObject=f;d[f]=d[f]||function(){(d[f].q=d[f].q||[]).push(arguments)},d[f].l=1*new Date();c=e.createElement(j),b=e.getElementsByTagName(j)[0];c.async=1;c.src=h;b.parentNode.insertBefore(c,b)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create","UA-100332682-2","auto");ga("send","pageview");</script>
<script type=text/javascript>document.oncontextmenu=new Function("return false");document.onselectstart=new Function("return false");if(window.sidebar){document.onmousedown=new Function("return false");document.onclick=new Function("return true");document.oncut=new Function("return false");document.oncopy=new Function("return false");document.onpaste=new Function("return false")};</script>
<!--[if lt IE 9]>
<script src=//html5shiv.googlecode.com/svn/trunk/html5.js></script>
<script src=https://oss.maxcdn.com/respond/1.4.2/respond.min.js></script>
<![endif]-->
<style>.nav-tabs>li{float:left;margin-bottom:-5px;border-bottom:transparent}</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
<section class="section_margin_gen_training section_training_banner pmi_class">
<div class=container>
<div class=row>
<div class="col-md-12 col-xs-12">
<h2 class=trainind_detail_title1>PMI- Agile Certified Practitioner (PMI-ACP)<sup>&reg;</sup> Preparatory Workshop</h2>
<h3 class=trainind_detail_title2>[ Master the art of Project Management with the agile principles and techniques ]</h3>
</div>
</div>
</div>
</section>
<section class=section_margin_gen_training>
<div class=container>
<div class=row>
<div class=col-md-12>
<h1 class="heading_trainings margin_top_gen">
Overview
</h1>
<p class=para_training>
This three-day PMI-Agile Certified Practitioner (PMI-ACP)&reg; preparatory workshop is designed with the objective to build your knowledge about agile principles and techniques. This course will enhance your professional versatility in project management tools and techniques by learning best practices from different agile methodologies.
</p>
<p class=para_training>
This workshop spans across many Agile approaches such as Scrum, Kanban, Lean, Extreme Programming (XP) and Test Driven Development (TDD) to gain the skills and knowledge required for PMI-ACP&reg; certification exam.
</p>
</div>
</div>
</div>
</section>
<section class="section_margin_gen_training section_training_requirements">
<div class=container>
<div class=row>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-hourglass-half fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Duration</h5>
<h6 class=training_requirement_title1>3 Days (24 Hours)</h6>
</div>
</div>
</div>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-clock-o fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Timing</h5>
<h6 class=training_requirement_title1>9:00 AM to 6:00 PM Each Day (1 Hour break in between)</h6>
</div>
</div>
</div>
<div class=border_seperator></div>
</div>
<div class=row>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-user-o fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Trainer</h5>
<h6 class=training_requirement_title1>Project Management & Agile Expert</h6>
</div>
</div>
</div>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-group fa_training glyphicon_edit" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Batch Size</h5>
<h6 class=training_requirement_title1>30 participants</h6>
</div>
</div>
</div>
<div class=border_seperator></div>
</div>
<div class=row>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-id-badge fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Participants Profile</h5>
<h6 class=training_requirement_title1>Intermediate</h6>
</div>
</div>
</div>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-check-circle-o fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Prerequisite</h5>
<h6 class=training_requirement_title1>None for attending Workshop. PMI-ACP&reg; exam prerequisites are mentioned on PMI website</h6>
</div>
</div>
</div>
<div class=border_seperator></div>
</div>
<div class=row>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-language fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Course Delivery Language</h5>
<h6 class=training_requirement_title1>English</h6>
</div>
</div>
</div>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<span class="glyphicon glyphicon-blackboard fa_training glyphicon_edit"></span>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Approach of Delivery</h5>
<h6 class=training_requirement_title1>Mix of theory & workshop sessions (Classroom Training)</h6>
</div>
</div>
</div>
<div class=border_seperator></div>
</div>
<div class=row>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-graduation-cap fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title_graduation>
<h5 class=training_requirement_title1>Certification Details</h5>
<h6 class=training_requirement_title1>Attending the class prepares you to take the exam to become a PMI - Agile Certified Practitioner (ACP)
</h6>
</div>
</div>
</div>
<div class="col-md-6 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-university fa_training fa-2x" aria-hidden=true></i>
</div>
<div class=training_requirement_title>
<h5 class=training_requirement_title1>Accreditation Institute</h5>
<h6 class=training_requirement_title1>Project Management Institute</h6>
</div>
</div>
</div>
<div class=border_seperator></div>
</div>
<div class=row>
<div class="col-md-12 col-xs-12">
<div class=training_detail_row>
<div class=training_requirement_icon>
<i class="fa fa-handshake-o fa_training fa-2x" aria-hidden=true></i>
</div>
<div class="training_requirement_title_full media_training_requirement">
<h5 class=training_requirement_title1>Commercials & Logistics</h5>
<h6 class=training_requirement_title1>Please contact us for quotation</h6>
</div>
</div>
</div>
</div>
</section>
<section class=section_margin_gen_training>
<div class=container>
<div class=row>
<div class=col-md-12>
<h1 class="heading_trainings margin_top_gen">
Agenda
</h1>
<ul>
<li class=training_li_list>Understand Agile Framework</li>
<li class=training_li_list>Planning, Monitoring and Adapting</li>
<li class=training_li_list>Value-Driven delivery</li>
<li class=training_li_list>Stakeholder engagement</li>
<li class=training_li_list>Problem detection and resolution</li>
<li class=training_li_list>Soft skills, metrics & Lean</li>
<li class=training_li_list>Continuous improvement.</li>
</ul>
</div>
<div class=col-md-12>
<h1 class="heading_trainings margin_top_gen">
Learning Objective
</h1>
<ul>
<li class=training_li_list>Introduction to Agile principles and practices</li>
<li class=training_li_list>Understand Project Management in the Agile context </li>
<li class=training_li_list>Understand the process of Agile project management </li>
<li class=training_li_list>Learn how to successfully apply Agile methods to projects and programs</li>
<li class=training_li_list>Learn how to guide high-performance team which delivers results</li>
<li class=training_li_list>Obtain valuable insights into how you can empower and inspire your team </li>
<li class=training_li_list>Learn how to apply powerful metrics to ensure continuous improvement </li>
<li class=training_li_list>Learn Agile concepts from real life examples from the other participants </li>
<li class=training_li_list>Gain powerful insights, techniques and skills to successfully coach a new or existing agile team. </li>
</ul>
</div>
<div class=col-md-12>
<h1 class="heading_trainings margin_top_gen">
Audience
</h1>
<ul>
<li class=training_li_list>Project Managers, Project Planners, Developers/Programmers, Designers, Testers, Project Controllers, Product Owners, Scrum Masters, Scrum Team Members</li>
</ul>
</div>
<div class=col-md-12>
<h1 class="heading_trainings margin_top_gen">
Course Deliverables
</h1>
<ul>
<li class=training_li_list>Participants have to take the PMI-ACP exam with pmi.org however we provide Certificate of Participation for the preparatory workshop. </li>
<li class=training_li_list>Attendee will get the printed copy of the work book</li>
<li class=training_li_list>24 PDUs and 24 SEUs</li>
<li class=training_li_list>In person training for three days full of learning and fun.</li>
</ul>
</div>
</div>
</div>
</section>
<section class="contact_us_bg_page no-margin-bottom margin_top_contact">
<div class=container>
<div class=row>
<div class="col-md-12 col-xs-12">
<h2 class=contact_training_title1>Want to Participate?</h2>
<h4 class=contact_training_title2>You can reach us to book in-house class</h4>
<a href=/contact/ class=training_contact>Contact</a>
</div>
</div>
</div>
</section>
<?php include('../../includes/footer.php'); ?>
<script src=../../js/jquery.js></script>
<script src=../../js/bootstrap.min.js></script>
</body>
</html>