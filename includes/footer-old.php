<?php 

    echo '

    <footer id="footer">
        <div class="scroll-to-top">
            <i class="fa fa-chevron-up"></i>
        </div>
        <div class="footer-closure">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <span class="copyrights">
                            &COPY;&nbsp;2013-
                            <script>document.write(new Date().getFullYear());</script>&nbsp;Innovation Roots Softech Pvt Ltd
                        </span>
                    </div>
                    <div class="col-md-6">
                        <div class="closure-nav">
                            <ul class="menu">
				
                                <li>
                                    <a href="/legal/terms-and-conditions">Terms and Conditions </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</footer>
    ';

    ?>

