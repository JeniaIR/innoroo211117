<?php 

    echo '
    <header id="header" class="navbar-fixed-top navbar-white navbar-transparent primary-nav" role="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="topbar-nav visible hidden-xs">
                        <ul class="pull-right">
                            <li>
                                Give Us a Call:&nbsp;
                                <i class="fa fa-phone"></i>
                                +(91) 84949 32221
                            </li>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <a href="mailto:training@innovationroots.com">training@innovationroots.com</a>
                            </li>
                            <li class="social-icon">
                                <a rel="tooltip" data-placement="bottom" title="Follow" target="_blank" href="//twitter.com/innoroo">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a rel="tooltip" data-placement="bottom" title="Follow" target="_blank" href="//www.linkedin.com/company/innovation-roots">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                                <a rel="tooltip" data-placement="bottom" title="Like" target="_blank" href="https://www.facebook.com/pages/Innovation-Roots/1040590309288148">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a rel="tooltip" data-placement="bottom" title="Follow" target="_blank" href="https://plus.google.com/u/2/b/105968218237342764016/105968218237342764016/posts">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                                	
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-header">
                        <!-- Toggle Navigation Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ir_nav">
                            <span class="sr-only">Toggle Menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-brand">
                            <a href="/">
                                <h2 class="logo-text">Innovation Roots</h2>
                            </a>
                        </div>
                    </div>
                    <nav id="ir_nav" class="collapse navbar-collapse clearfix" role="navigation">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="/training/">Training</a>
                            </li>
                            <li>
                                <a href="/careers/">Careers</a>
                            </li>
                            <li>
                                <a href="/contact/">Contact</a>
                            </li>
                            <li>
                                <a href="/blog">Blog</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>';

    ?>
