<!DOCTYPE html>
<html lang="en">


<head>

<meta charset="utf-8">
<title>Lean Kanban India - 2017</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Lean Kanban India" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />	
<link href="../css/cubeportfolio.min.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<!-- Theme skin -->
<link id="t-colors" href="../skins/yellow.css" rel="stylesheet" />

<link rel="icon" href="../img/favicon.png" sizes="32x32" />

<link rel="icon" href="../img/favicon.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="img/favicon.png" />
	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<style>
/*LKIN Contact us-----------------------------------*/

.contact_lkin {
    background-color: #f2f2f2;
    padding: 0px;

}
.contact_titles
{
    text-align: center !important;
    color:#000 !important;
    margin-bottom:0px;
}

.contact_des {

    border-radius: 4px;
    z-index: 1;
    padding: 20px;
    width: 85%;
    margin: 0 auto;
    min-height: 401px;
    background-color:#fff;
    box-shadow: 0 4px 8px 0 rgba(224, 224, 224, 0.39), 0 6px 20px 0 rgba(216, 212, 212, 0.45);
}
.contact_person {
    color: #000;
    font-size: 21px;
}
.input_contact {
    outline: none;
    border: none;
    border-bottom: 1px solid #b9b9b9;
    margin: 2% 0px;
    width: 100%;
    /* color: red; */
    border-radius: 3px;
    padding: 10px 0;
}
.input_contact_captcha
{
    outline: none;
    border: none;

    margin: 2% 0px;
    width: 20%;
    /* color: red; */
    border-radius: 3px;
    padding: 10px;   
}
.fa_captcha {
    color: #000;
    padding: 0 5px;
}

.btn_contact {
    background-color: #4885ed;
    border-radius: 0px;
    padding: 5px 15px;
    border: none;
    outline: none;
}
.btn_contact:hover {
    background-color: #4885ed;    
}
.spam_mail {
    font-size: 13px;
    padding: 4px 0;
}
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>



<div id="wrapper">
	<!-- start header -->
	<header>
			
			
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                          
                   <div class="col-xs-8 col-sm-12 col-lg-12">
	                    <a class="navbar-brand" href="http://leankanbanindia.com/">
	                     <img src="../img/logo.png" alt="Lean Kanban India" style="max-width:100%;" class="img-responsive" />
	                    </a>
                    </div>
                    <div class="col-xs-4">
	                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </button>
	                 </div>
                </div>
                <div class="navbar-collapse collapse ">
                     <ul class="nav navbar-nav">
                    
                   	 <li class="li-secondary"><a href="http://leankanbanindia.com/#cfp">CAll FOR PROPOSAL</a></li>
                   	 <li class="li-secondary"><a href="http://www.leankanbanindia.com/program_schedule/">Program Schedule</a></li>
                   	 <li class="li-secondary"><a href="#">SPEAKERS</a></li>
                   	 <li class="li-secondary"><a href="http://leankanbanindia.com/#workshop">WORKSHOPS</a></li>
                         <li class="li-secondary"><a href="http://leankanbanindia.com/#sponsor">SPONSOR</a></li>
                        
                        
                        
                         <li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Past Events <i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu">
	                                <!--<li><a href="http://www.leankanbanapac.com/conference-india.html" target="_blank">LKIN 2014</a></li>-->
	                                <li><a href="http://lkin15.leankanban.com/" target="_blank">LKIN 2015</a></li>
					<li><a href="http://www.leankanbanindia.com/lkin16.leankanban.com/" target="_blank">LKIN 2016</a></li>
					
                                </ul>
			</li>
			<!--<li class="li-secondary"><a target="blank" href="https://goeventz.com/event/lean-kanban-india-2017/39868">REGISTER</a></li>-->
                   
                    </ul>
                    
                    
                    
                    
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
    
		
	
		
	
	   
		
		     <div class="clear-fix"></div>
		     
	<section class="contact_lkin" >
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact_bg">
                            <p class="cfp-title contact_titles" >Contact Us
                            </p>
                            <p class="cfp-para1 contact_titles" style="text-align:center !important;">Contact us for more information and group discounts</p>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact_des">
                            <form name="contactform" method="post" action="contact.php" id="registerForm">

                                <input class="input_contact" type="text" id="name" name="name" maxlength="50" size="30" placeholder="Your Name *" required>
                                <br>
                                <input class="input_contact"  type="email" id="email" name="email" maxlength="80" size="30" placeholder="email *" required>
                                <br>
                                
                                <textarea class="input_contact"  type="text" id="message" name="comment" maxlength="80" size="30" placeholder="Message *" required></textarea>
                                <!--<br>
                                <input class="input_contact_captcha" type="text" id="captcha" /><i class="fa fa-refresh fa_captcha refresh" aria-hidden="true"></i>
                                <br>
                                <input class="input_contact" placeholder="Enter the captcha here" type="text" id="inputText"/> -->
                                <div class="g-recaptcha" data-sitekey="6LeYjSwUAAAAAPa7GI7fylKYYedsJH9PB6hXAIwF"></div>
                        	<span style="display:none; color:#ff00000" id="captchamsg">Please click on Captcha</span>
                                <br>
                                <input type="submit" onclick="return check()" value="Submit"  class="btn btn-primary btn_contact">  
                                <p class="spam_mail">No spam mails. We promise!</p>

                            </form>
                        </div>
                        <script>
                            var captcha;

                            function generateCaptcha() {
                                var a = Math.floor((Math.random() * 10));
                                var b = Math.floor((Math.random() * 10));
                                var c = Math.floor((Math.random() * 10));
                                var d = Math.floor((Math.random() * 10));

                                captcha = a.toString() + b.toString() + c.toString() + d.toString();

                                document.getElementById("captcha").value = captcha;
                            }

                            function check() {
                            
                              
                               var name = document.getElementById("name").value;
                              
                                if($.trim(name) =='')
                                {
                                  alert("Please Enter Valid Name.");
                                  return false;
                                }
                                
                                var email = document.getElementById("email").value;
                              
                                if($.trim(email) =='')
                                {
                                  alert("Please Enter Valid Email.");
                                  return false;
                                }
                                var message = document.getElementById("message").value;
                              
                                if($.trim(message) =='')
                                {
                                  alert("Please Enter message.");
                                  return false;
                                }
                                
                                
                              var fdata = $('#registerForm').serialize();
		                var returnvalue = false;
		               
		                
		               $.ajax({
		              type: "POST",
		              url: "gcaptcha.php",
		              async: false,
		              data: fdata,
		              //contentType: "text/json; charset=utf-8",
		              //dataType: "text",
		              success: function (msg) {
		                  
		                  
		                  if(msg.localeCompare("failure") == 0)                
		                  {
		                    
		                    $('#captchamsg').css('color', 'red');
		                    $('#captchamsg').show();   
		                    
		                     returnvalue  = false;             
		                  }
		                  else
		                  {
		                  $('#captchamsg').hide();    
		                  returnvalue = true;
		                               
		                  }                  
		                }    
                                 
                                
                                
                            });
                             
                             return returnvalue ;

		}




                        </script>
                    </div>
                    <div class="col-md-5">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.657984076467!2d77.68090971482154!3d12.929689990883386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae13a6c35ad863%3A0xc9976c935f42f551!2sNovotel+Bengaluru+Techpark!5e0!3m2!1sen!2sin!4v1497852680092" height="450"  style="border:0; width:100%; box-shadow: 0 4px 8px 0 rgba(187, 187, 187, 0.39), 0 6px 20px 0 rgba(187, 187, 187, 0.45);" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

        </section>
		               
		
	    
	    
	    
		
		
    
    
    		
		
	
	<div id="divider" class="inview" style="background-position: 0% 0px; background-color:#f2f2f2;">
		<div class="divider-bg" style="background-position: 70% -1754.4px;"></div>

	 	 <div class="container">
	  
			<div class="row">
	        		<div class="col-12-lg social-media">
	        
			    		<div class="social-icons">
			    		
			                <a class="social-media facebook" href="https://www.facebook.com/leankanbanindia/" target="_blank" ><i class="fa fa-facebook-square fa-4x"></i></a>
							<a class="social-media white twitter" href="https://www.twitter.com/LeanKanbanIndia" target="_blank"><i class="fa fa-twitter-square fa-4x"></i></a>
							<a class="social-media white linkedin" href="https://www.linkedin.com/groups/8358254" target="_blank"><i class="fa fa-linkedin-square fa-4x"></i></a>
							
			              
			              </div>  
	              
	        		</div>
			</div>
	  
	  	</div>

	</div>
	
	<footer style="padding-top:0px;">
	
	
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="col-md-6 col-sm-6 col-xs-12">
		                        <!--<p class="footerlogo"> LEAN KANBAN INDIA</p>-->
		                    	</div>
				</div>		
				
			</div>
		
		
		</div>
		<div id="sub-footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="copyright">
							<p>Manager and Organiser :  <a href="http://www.innovationroots.com/" >INNOVATION ROOTS</a> </p>
						<div class="clearfix"></div>	
							
						<!--<div id="seocontentbox">			
							<p>We are a end-to-end Agile Software Development Solution provider and consultancy, having thousands of consulting hours. We are bunch of passionate individuals who believes transformation begins through community and Lean Kanban India conference is one of these initiatives in creating a positive social change.</p>		
						</div>	-->
					
	                       
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</footer>

<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>


<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery.min.js"></script>
<script src="../js/modernizr.custom.js"></script>
<script src="../js/jquery.easing.1.3.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../plugins/flexslider/jquery.flexslider-min.js"></script> 
<script src="../plugins/flexslider/flexslider.config.js"></script>
<script src="../js/jquery.appear.js"></script>
<script src="../js/stellar.js"></script>
<script src="../js/classie.js"></script>
<script src="../js/uisearch.js"></script>
<script src="../js/jquery.cubeportfolio.min.js"></script>
<script src="../js/google-code-prettify/prettify.js"></script>
<script src="../js/animate.js"></script>
<script src="../js/custom.js"></script>



	
</body>
</html>