<!DOCTYPE html>
<html lang="en">





    <head>

        <meta charset="utf-8">
        <title>Lean Kanban India - 2017</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Lean Kanban India" />
        <!-- css -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" />
        <link href="../plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />	
        <link href="../css/cubeportfolio.min.css" rel="stylesheet" />
        <link href="../css/style.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <!-- Theme skin -->
        <link id="t-colors" href="../skins/yellow.css" rel="stylesheet" />

        <link rel="icon" href="../img/favicon.png" sizes="32x32" />

        <link rel="icon" href="../img/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="img/favicon.png" />
        <!-- boxed bg -->
        <link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <style>
            /*LKIN Contact us-----------------------------------*/

            .contact_lkin {
                background-color: #f2f2f2;
                padding: 0px;

            }
            .contact_titles
            {
                text-align: center !important;
                color:#000 !important;
                margin-bottom:0px;
            }

            .contact_des {

                border-radius: 4px;
                z-index: 1;
                padding: 20px;
                width: 85%;
                margin: 0 auto;
                min-height: 401px;
                background-color:#fff;
                box-shadow: 0 4px 8px 0 rgba(224, 224, 224, 0.39), 0 6px 20px 0 rgba(216, 212, 212, 0.45);
            }
            .contact_person {
                color: #000;
                font-size: 21px;
            }
            .input_contact {
                outline: none;
                border: none;
                border-bottom: 1px solid #b9b9b9;
                margin: 2% 0px;
                width: 100%;
                /* color: red; */
                border-radius: 3px;
                padding: 10px 0;
            }
            .input_contact_captcha
            {
                outline: none;
                border: none;

                margin: 2% 0px;
                width: 20%;
                /* color: red; */
                border-radius: 3px;
                padding: 10px;   
            }
            .fa_captcha {
                color: #000;
                padding: 0 5px;
            }

            .btn_contact {
                background-color: #4885ed;
                border-radius: 0px;
                padding: 5px 15px;
                border: none;
                outline: none;
            }
            .btn_contact:hover {
                background-color: #4885ed;    
            }
            .spam_mail {
                font-size: 13px;
                padding: 4px 0;
            }

.fa-gen {
                        font-size: 19px;
                    }
 a.a_fa_gen:hover
                    {color: #fff !important;}
 a.a_fa_gen:active
                    {color: #fff !important;}
 a.a_fa_gen:hover:focus
                    {color: #fff !important;}



  .hotel_contact_img {
                        box-shadow: 0 4px 8px 0 rgba(171, 170, 170, 0.39), 0 6px 20px 0 rgba(167, 167, 167, 0.45);
                        width: 100%;
                        margin: 2% 0;
                    }
                    .info_tab {
                        border: 1px solid #bbbbbb;
                        width: 40%;
                        margin: 0 auto;
                        padding: 10px;
                    }
                    .text_location_bar
                    {
                        text-align: center;
                        margin: 0px;
                    }

                    .location_sub_title_tab {
                        text-align: center;
                        color: #fff;
                        background: #ff7500;
                        font-size: 40px;
                        font-weight: 300;
                        padding: 20px 0;
                        line-height: 50px;
                        /* margin: 0px; */
                    }
                    a.a_know_more_hotel {
                        text-decoration: none;
                        color: #4885ed;
                        font-weight: 600;
                    }
                    .location_info_wrapper {
                        font-size: 18px;
                        line-height: 29px;
                        font-weight: 300;
                        /* text-align: center; */
                    }
                    a.know_more_location_city {
                        text-decoration: none;
                        color: #4885ed;
                        font-weight: 600;
                        padding: 20px
                    }
                    .know_more_location_main {
                        width: 100%;
                        text-align: center;
                    }
                    .fa_location {
                        padding: 0 23px;
                        color: #ff7500;
                        font-size: 21px;
                    }
                    .row_location
                    {
                        margin: 1%;
                        padding: 20px 0;
                        min-height: 77px;
                        background: rgb(247, 240, 230);
                    }
                    @media (min-width:0px) and (max-width:768px)
                    {
                        .fa_location {
                            padding: 0px;

                        }
                        .location_info_wrapper {
                            font-size: 16px;
                          
                        }
                    }



        </style>
    </head>
    <body onload="generateCaptcha()">



        <div id="wrapper">
            <!-- start header -->
               <header>
			
		<div class="primary-nav-bar">
                    <div class="container">
                        <div class="TopMenuBar">
                            <ul class="float_ul_primary">
                                <!--<li><a href="#">Webinar</a></li>
                                <li><a href="#">Meetup</a></li>-->
                                <li class="dropdown"><a href="#" class="a_hover_primary">Past Conferences</a>
                                    <div class="dropdown-menu dropdown_media">
                                        <ul>
                                            <li><a href="http://lkin15.leankanban.com/" target="blank" class="a_primary_sub">LKIN15</a></li>
                                            <li class="sub-dropdown"><a href="../conference/2016/" target="blank" class="a_primary_sub">LKIN16</a>

                                            </li>

                                        </ul>
                                    </div>
                                </li>
  <li>
                                    <a class="facebook a_fa_gen" href="https://www.facebook.com/leankanbanindia/" target="_blank" ><i class="fa fa-facebook-square  fa-gen "></i></a>
                                </li>
                                <li>
                                    <a class="twitter a_fa_gen" href="https://www.twitter.com/LeanKanbanIndia" target="_blank"><i class="fa fa-twitter-square  fa-gen "></i></a>

                                </li>

                                <li>
                                    <a class="linkedin a_fa_gen" href="https://www.linkedin.com/groups/8358254" target="_blank"><i class="fa fa-linkedin-square   fa-gen "></i></a>

                                </li>
                                <li>
                                    <a class="youtube a_fa_gen" href="https://www.youtube.com/channel/UChgtAd7mcKtu34R-NuSi7Vw" target="_blank"><i class="fa fa-youtube-play   fa-gen "></i></a>

                                </li>
                            </ul>
                        </div>


                    </div>

                </div>	
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                          
                   <div class="col-xs-8 col-sm-12 col-lg-12">
	                    <a class="navbar-brand" href="http://leankanbanindia.com/">
	                     <img src="../img/logo.png" alt="Lean Kanban India" style="max-width:100%;" class="img-responsive" />
	                    </a>
                    </div>
                    <div class="col-xs-4">
	                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </button>
	                 </div>
                </div>
                <div class="navbar-collapse collapse ">
                     <ul class="nav navbar-nav">
                    
                   	 <!-- <li class="li-secondary"><a href="#cfp">CAll FOR PROPOSAL</a></li>-->
                   	 <li class="li-secondary"><a href="http://www.leankanbanindia.com/program_schedule/">Program Schedule</a></li>
                   	 <li class="li-secondary"><a href="http://leankanbanindia.com/#speakers">SPEAKERS</a></li>
 <li class="li-secondary"><a href="http://leankanbanindia.com/abstract/">ABSTRACT</a></li>
                   	 <li class="li-secondary"><a href="http://leankanbanindia.com/#workshop">WORKSHOPS</a></li>
                         <li class="li-secondary"><a href="http://leankanbanindia.com/#sponsor">SPONSOR</a></li>
                                               <li class="li-secondary"><a href="http://leankanbanindia.com/venue/">VENUE</a></li>
                        
                        
                        <!-- <li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Past Events <i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu">
	                                
	                                <li><a href="http://lkin15.leankanban.com/" target="_blank">LKIN 2015</a></li>
					<li><a href="http://www.leankanbanindia.com/lkin16.leankanban.com/" target="_blank">LKIN 2016</a></li>
					
                                </ul>
			</li>-->
			<!--<li class="li-secondary"><a target="blank" href="https://goeventz.com/event/lean-kanban-india-2017/39868">REGISTER</a></li>-->
                   
                    </ul>
                    
                    
                    
                    
                </div>
            </div>
        </div>
	</header>
            <!-- end header -->


            <section class="contact_lkin"style="padding:0px;">
               
           <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="location_title_bar">
                                <p class="cfp-title" style="margin-bottom:0px;">Venue</p>
                            </div>

                            <!-- <div class=" info_tab">
                                 <p class="text_location_bar">
                                     <span style="color: #808080;">
                                         <a style="color: #808080;   text-decoration: none;" href="#hotel-info">Hotel Info</a> ~ 
                                         <a style="color: #808080;   text-decoration: none;" href="#travel-info">Travel Info </a>~ 
                                         <a style="color: #808080;   text-decoration: none;" href="#city-info">City Info</a>
                                     </span>
                                 </p>
                             </div>-->

                        </div>

                        <div class="col-md-12">
                            <div class="wpb_wrapper" id="hotel-info">
                                <!-- <h2 class="location_sub_title_tab">HOTEL INFO</h2>-->
                                <p class="text_intro_venue">Conference would be held at Novotel Bengaluru Techpark, Bengaluru.</p>
                                <div class="locatio_hotel_img">

                                    <img src="images/novotel.jpg" alt=""  class="img-responsive hotel_contact_img"/>

                                </div>

                                <div class="location_info_wrapper">
                                    <div class="location_info_wrapper">
                                        <div class="row">
                                            <div class="row_location"style="background: #f2f2f2;margin: 0 1%;min-height: 110px;">
                                                <div class="col-md-4     ">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>Hotel</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                         <a href="http://www.novotel.com/gb/hotel-6453-novotel-bengaluru-techpark/index.shtml" target="blank"style="color:#000;text-decoration:none;" > Novotel Bengaluru Techpark</a> and IBIS Bengaluru Techpark have multiple options for lodging and boarding. Hilton Suits Bellandur has discount deal for LKIN17, reach us for details
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row_location">
                                                <div class="col-md-4 ">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>Airport</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                       <a href="http://www.bengaluruairport.com/home.jspx?_afrLoop=4388245564953722&_afrWindowMode=0&_adf.ctrl-state=11p6rik5ru_169" target="blank"style="color:#000;text-decoration:none;" >Kempegowda International Airport</a> is the nearest airport to reach Bengaluru by air transport
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row_location"style="background: #f2f2f2;margin: 0 1%;">
                                                <div class="col-md-4     ">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>Airport Shuttle Services</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                        There are multiple options like Ola, Uber taxi services and BMTC Vayu Vajra bus services to reach Bengaluru city and the hotel
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row_location">
                                                <div class="col-md-4 ">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>City Taxi</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                        Ola and Uber are easily available all across Bengaluru city
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row_location"style="background: #f2f2f2;margin: 0 1%;">
                                                <div class="col-md-4">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>City Bus Services</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                        AC/Non-AC BMTC city buses are easily available all across Bengaluru for a ticket or daily pass
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row_location" style="    min-height: 130px;">
                                                <div class="col-md-4">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>Rail Transport</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                        Krantiveer Sangolli Rayanna Railway Station, Bengaluru Cantonment railway station, Yeshwantapur junction and Krishnarajapuram railway station are the major railway station to travel by train
                                                    </p>
                                                </div>
                                            </div> 
                                            <div class="row_location"style="background: #f2f2f2;margin: 0 1%;">
                                                <div class="col-md-4">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>Road Transport</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                        The main bus station that is the Kempegowda Bus Station, locally known as "Majestic bus station”
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row_location">
                                                <div class="col-md-4 ">
                                                    <p  class="location_info_wrapper">
                                                        <i class="fa fa-chevron-right fa_location " aria-hidden="true"></i>
                                                        <strong>Metro Services</strong></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="location_info_wrapper">
                                                        Namma Metro/Bengaluru Metro is the metro system serving the city of Bengaluru
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="wpb_wrapper" id="city-info">
                                <div class="location_title_bar">
                                    <p class="cfp-title" style="margin-bottom:0px;">Explore Bengaluru</p>
                                </div>
                                <div class="col-md-6"  
                                     <div class="locatio_hotel_img">

                                        <img src="images/city.jpg" alt=""  class="img-responsive hotel_contact_img"/>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="location_info_wrapper">
                                        <p style="text-align:justify;    padding: 10px 0 0 0;">Bengaluru is referred to as the Silicon Valley of India and accounts for 35 percent of India's software exports. Bengaluru known as "The Garden City" located on the Deccan Plateau in the south-eastern part of Karnataka. Bengaluru evolved into an industrial hub for public sector heavy industries, particularly aerospace, telecommunications, machine tools, heavy equipment for space and defense.</p>
                                    </div>
                                    <a target="blank" href="https://www.tripadvisor.in/Attractions-g297628-Activities-Bengaluru_Bangalore_District_Karnataka.html" class="know_more_location_city"> 
                                        <span class="know_more_workshop_main">KNOW MORE&nbsp;&nbsp;<i class="fa fa fa-arrow-circle-right i_workshop" aria-hidden="true"></i></span></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>












           <!-- <div id="divider" class="inview" style="background-position: 0% 0px;">
                <div class="divider-bg" style="background-position: 70% -1754.4px;"></div>

                <div class="container">

                    <div class="row">
                        <div class="col-12-lg social-media">

                            <div class="social-icons">

                                <a class="social-media facebook" href="https://www.facebook.com/leankanbanindia/" target="_blank" ><i class="fa fa-facebook-square fa-4x"></i></a>
                                <a class="social-media white twitter" href="https://www.twitter.com/LeanKanbanIndia" target="_blank"><i class="fa fa-twitter-square fa-4x"></i></a>
                                <a class="social-media white linkedin" href="https://www.linkedin.com/groups/8358254" target="_blank"><i class="fa fa-linkedin-square fa-4x"></i></a>


                            </div>  

                        </div>
                    </div>

                </div>

            </div>-->

            <footer style="padding-top:0px;">


                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <!--<p class="footerlogo"> LEAN KANBAN INDIA</p>-->
                            </div>
                        </div>		

                    </div>


                </div>
                <div id="sub-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="copyright">
                                    <p>Manager and Organiser :  <a href="http://www.innovationroots.com/" >INNOVATION ROOTS</a> </p>
                                    <div class="clearfix"></div>	

                                    <!--<div id="seocontentbox">			
                                            <p>We are a end-to-end Agile Software Development Solution provider and consultancy, having thousands of consulting hours. We are bunch of passionate individuals who believes transformation begins through community and Lean Kanban India conference is one of these initiatives in creating a positive social change.</p>		
                                    </div>	-->


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </footer>

            <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>


            <!-- Placed at the end of the document so the pages load faster -->
            <script src="../js/jquery.min.js"></script>
            <script src="../js/modernizr.custom.js"></script>
            <script src="../js/jquery.easing.1.3.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../plugins/flexslider/jquery.flexslider-min.js"></script> 
            <script src="../plugins/flexslider/flexslider.config.js"></script>
            <script src="../js/jquery.appear.js"></script>
            <script src="../js/stellar.js"></script>
            <script src="../js/classie.js"></script>
            <script src="../js/uisearch.js"></script>
            <script src="../js/jquery.cubeportfolio.min.js"></script>
            <script src="../js/google-code-prettify/prettify.js"></script>
            <script src="../js/animate.js"></script>
            <script src="../js/custom.js"></script>




    </body>
</html>