<?php
/* *
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package sparkling
 */

if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!doctype html>
<!--[if !IE]>
<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="<?php echo of_get_option( 'nav_bg_color' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="/inc/assets/css/style.css">


</head>

<body <?php body_class(); ?>>
<a class="sr-only sr-only-focusable" href="#content">Skip to main content</a>
<div id="page" class="hfeed site">

	<header >
		<div class="container-fluid main" style="margin-bottom:100px;">

                <nav class="navbar navbar-default navbar-fixed-top" style="position: fixed ;">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle toggle_edit_mobile" data-toggle="collapse" data-target="#ir_nav">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span> 
                            </button>
                            <div class="navbar-brand">
                                <a href="http://innovationroots.com/">
                                    <h2 class="logo-text">Innovation Roots</h2>
                                </a>
                            </div>
                            
                        </div>
                        <div class="collapse navbar-collapse" id="ir_nav">
                            <ul class="nav navbar-nav ul_list_header">
                                <li ><a href="http://www.innovationroots.com/training/" class="li_list_header">Training</a></li>
                                
 
                                <li ><a href="http://www.innovationroots.com/calendar/" class="li_list_header">Calendar</a></li>
                                <li ><a href="http://www.innovationroots.com/contact/" class="li_list_header">Contact</a></li>

                                <li ><a href="http://www.innovationroots.com/blog/" class="li_list_header">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>





            </div><!-- .site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">

		<div class="top-section">
			<?php sparkling_featured_slider(); ?>
			<?php sparkling_call_for_action(); ?>
		</div>

		<div class="container main-content-area">
            <?php $layout_class = get_layout_class(); ?>
			<div class="row <?php echo $layout_class; ?>">
				<div class="main-content-inner <?php echo sparkling_main_content_bootstrap_classes(); ?>">
